'use strict'

const btns = document.getElementsByClassName('btn');

const checkKeyDown = (event) => {
    for (let i = 0; i < btns.length; i++) {
        if (event.key === btns[i].innerText || event.key.toUpperCase() === btns[i].innerText) {
            btns[i].style.backgroundColor = 'blue';
        }
    }
}
const checkKeyUp = (event) => {
    for (let i = 0; i < btns.length; i++) {
        if (event.key === btns[i].innerText || event.key.toUpperCase() === btns[i].innerText) {
            btns[i].style.backgroundColor = 'black';
        }
    }
}

document.addEventListener('keydown', checkKeyDown);
document.addEventListener('keyup', checkKeyUp);