'use strict'

const showListOnPage = (elem, dom) => {
    if (Array.isArray(elem)) {
        const newUl = document.createElement('ul');
        elem.map((elem2) => showListOnPage(elem2, newUl));
        dom.append(newUl);
        return newUl;
    }
    const newLi = document.createElement('li');
    newLi.innerHTML = elem;
    dom.append(newLi);
    return newLi;
}

const clearNode = (time, node) => {
    const newP = document.createElement('p');
    newP.style.fontSize = '50px';
    newP.style.color = '#ff0000';
    newP.style.textAlign = 'center';

    if (time > 0) {
        newP.innerText = 'Clean after: ' + time-- + 's';
        node.append(newP)
    } else {
        node.innerHTML = '';
    }
    let interval = setInterval(() => {
        if (time > 0) {
            newP.innerText = 'Cleaning after: ' + time-- + 's';
            node.append(newP)
        } else {
            node.innerHTML = '';
            clearInterval(interval)
        }
    }, 1000)
}

const newDiv = document.createElement('div');
newDiv.style.backgroundColor = '#0000ff';
document.body.append(newDiv);
document.body.style.color = '#ffffff';
showListOnPage(["Kharkiv", "Kiev", ["Borispol", "Irpin", ["Odessa", "Lviv"]],"Dnieper"], newDiv);
clearNode(3, document.body);