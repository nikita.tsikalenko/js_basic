const checkRightInputNumber = (message) => {
    for (let i = 0; true; i++) {
        const num = prompt(message);
        if (!! num && !Number.isNaN(+num)) {
            return +num;
        }
        alert('Wrong input number');
    }
}

const student = {
    _name: undefined,
    _lastName: undefined,
    table: {
        addDiscipline() {
            while (true) {
                let disc = prompt('Input discipline');
                if (disc === null) {
                    break;
                }
                this[disc] = checkRightInputNumber('Input mark');
            }
        },
        countBadMark() {
            let counter = 0;
            for (let key in this) {
                let mark = +this[key];
                if (!isNaN(mark) && mark > 2) {
                    counter++;
                }
            }
            return counter;
        },
        avgMark() {
            let counter = 0;
            let sum = 0;
            for (let key in this) {
                let mark = +this[key];
                if (!isNaN(mark)) {
                    counter++;
                    sum += Number.parseInt(this[key]);

                }
            }
            return sum / counter;
        },
    },
    set name(value) {
        this._name = value;
    }
    ,
    set lastName(value) {
        this._lastName = value;
    }
    ,
    checkTransfer() {
        if (this.table.countBadMark() < 4) {
            console.log('Student transferred to the next course');
        }
    }
    ,
    checkGrant() {
        if (this.table.avgMark() > 7) {
            console.log('Student awarded a grant');
        }
    }
}
student.name = prompt('Input name');
student.lastName = prompt('Input last name');
student.table.addDiscipline();
student.checkTransfer();
student.checkGrant();