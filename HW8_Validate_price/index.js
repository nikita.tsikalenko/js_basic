'use strict'

const label = document.createElement('label');
label.innerText = 'Price';
label.style.marginRight = '10px';

const input = document.createElement('input');

const span = document.createElement('span');

const buttonX = document.createElement('button');
buttonX.innerText = 'x';

const filter = document.createElement('div');
filter.append(span, buttonX)

const incorrect = document.createElement('p');
incorrect.innerText = 'Please enter correct price';
incorrect.style.color = 'red';

input.addEventListener('focusin', () => {
    input.style.borderWidth = '2px';
    input.style.borderColor = 'green';
    input.style.color = 'black';

});

input.addEventListener('focusout', () => {
    if(+input.value > 0) {
        incorrect.remove();
        input.style.borderWidth = '0';
        span.innerText = input.value;
        document.body.prepend(filter);
        input.style.color = 'green';
    } else {
        input.style.borderColor = 'red';
        input.style.color = 'red';
        filter.remove();
        input.after(incorrect);
    }
});

buttonX.addEventListener('click', () => {
    input.value = '';
    filter.remove();
})

document.body.append(label, input);