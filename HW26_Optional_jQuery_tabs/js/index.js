'use strict'

$('.tabs-title').click(function (){
    $('.tabs-title').removeClass('active')
    $(this).addClass('active')
    $('.text').removeClass('show');
    $('.' + $(this).attr('id')).addClass('show');
})