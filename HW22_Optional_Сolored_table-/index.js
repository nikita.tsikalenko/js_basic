'use strict'

const drowTable = (row, column) => {
    const table = document.createElement('table');
    for (let i = 0; i < row; i++) {
        const row = document.createElement('tr');
        row.style.height = '20px';
        for (let j = 0; j < column; j++) {
            const td = document.createElement('td');
            td.style.width = '20px';
            row.append(td);
        }
        table.append(row);
    }
    return table;
}

document.body.append(drowTable(30, 30));

const changeColor = (event) => {
    if (event.target.tagName === 'TD') {
        const td = event.target;
        td.classList.toggle('black')
        event.stopPropagation()
    }
}

const changeAllColor = () => {
    debugger
    document.body.querySelector('table').classList.add('test');
}

document.body.querySelector('table').addEventListener('click', changeColor,);
// document.body.querySelector('td').style.backgroundColor = !document.body.querySelector('td').style.backgroundColor;
document.body.addEventListener('click', changeAllColor);