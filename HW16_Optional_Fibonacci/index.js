const fibonacci = (f0, f1, n) => {
    if (n !== 0) {
        if (n > 0) {
            return fibonacci(f1, f0 + f1, n - 1);
        }
        return fibonacci(f1 - f0, f0, n + 1)
    }
    if (f1 > 0 && f0 > 0){
        return f1;
    }
    return f0;

}

const checkRightInputNumber = (message) => {
    for (let i = 0; true; i++) {
        const num = prompt(message);
        if (!!num && !Number.isNaN(+num)) {
            return +num;
        }
        alert('Wrong input number');
    }
}

alert(fibonacci(1, 1, checkRightInputNumber('Input position of fibonacci number')));