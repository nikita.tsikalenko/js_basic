const copyObj = (obj, newObj) => {
    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            if (Array.isArray(obj[i])) {
                newObj[i] = copyObj(obj[i], [])
                continue;
            } else if (typeof (obj[i]) === 'object' && obj[i] !== null) {
                newObj[i] = copyObj(obj[i], {})
                continue;
            }
            newObj[i] = obj[i];
        }
    } else if (typeof (newObj) === 'object' && newObj !== null) {
        for (const key in obj) {
            if (Array.isArray(obj[key])) {
                newObj[key] = copyObj(obj[key], [])
                continue;
            } else if (typeof (obj[key]) === 'object' && obj[key] !== null) {
                newObj[key] = copyObj(obj[key], {});
                continue;
            }
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

const obj = {a: 1, b: [2, 3, {f: 5, g: {n: 6, k: [7, [8,9]]}}], e: 4}
const newObj = copyObj(obj, {});
console.log(newObj);