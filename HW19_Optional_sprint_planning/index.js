'use strict'
const sprintPlanning = (emp, beckLog, dayOfStart ,deadline) => {
    let eStoryPointPerDay = 0;
    for (let i = 0; i < emp.length; i++) {
        eStoryPointPerDay += emp[i];
    }
    let bStoryPoint = 0;
    for (let i = 0; i < beckLog.length; i++) {
        bStoryPoint += beckLog[i];
    }
    const timeForSolution = bStoryPoint / eStoryPointPerDay;
    let day = dayOfStart;
    for(let i = timeForSolution; i > 0;){
        if(day.getDay() !== 6 || day.getDay() !== 0){
            if(i > 0 && i < 1){
                i.toFixed(2)
                day = new Date (day.getTime() + i * 24 * 3600 * 1000);
                break;
            }
            i--;
        }
        day = new Date (day.getTime() + 24 * 3600 * 1000);

    }

    const time =  day.getTime() - deadline.getTime();

    if (time < 0) {
        console.log('Все задачи будут успешно выполнены за ' + parseInt(String(-time / 24 / 3600 / 1000)) + ' дней до наступления дедлайна!');
        return true;
    }
    console.log('Команде разработчиков придется потратить дополнительно ' + parseFloat( String(time / 3600 / 1000 / 3)).toFixed(1) + ' часов после дедлайна, чтобы выполнить все задачи в беклоге')
    return false;
}

sprintPlanning([4, 6], [11, 9, 20, 9], new Date(2022, 0, 29), new Date(2022, 1, 2));