'use strict'

const darkScheme = () => {
    document.body.classList.toggle('dark-scheme');
    document.body.getElementsByClassName('text')[0].classList.toggle('dark-scheme');
    document.body.getElementsByClassName('text')[1].classList.toggle('dark-scheme');
    document.body.getElementsByClassName('text-title')[0].classList.toggle('dark-scheme');
    document.body.getElementsByClassName('copyright')[0].classList.toggle('dark-scheme');
    if (document.body.classList.contains('dark-scheme')){
        localStorage.setItem('color-scheme', 'dark-scheme');
    } else {
        localStorage.setItem('color-scheme', 'light-scheme');
    }
}


window.addEventListener('load', ()=>{
    if(localStorage.getItem('color-scheme')=== 'dark-scheme'){
        darkScheme();
    }
})
document.body.getElementsByClassName('color-scheme')[0].addEventListener('click', darkScheme)



