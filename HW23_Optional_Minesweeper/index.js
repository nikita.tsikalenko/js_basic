'use strict'

const drowTable = (row, column) => {
    const table = document.createElement('table');
    table.classList.add('game-field');
    for (let i = 0; i < row; i++) {
        const row = document.createElement('tr');
        row.classList.add(String(i + 1 + 'row'));
        for (let j = 0; j < column; j++) {
            const cell = document.createElement('td');
            cell.classList.add(String(j + 1 + 'col'));
            row.append(cell);
        }
        table.append(row);
    }
    return table;
}

const setMines = (table) => {
    let mines = table.getElementsByTagName('td').length / 6;
    const row = table.childElementCount;
    const col = table.firstElementChild.childElementCount;
    while (mines > 0) {
        const random = Math.round(Math.random() * (row * col - 1));
        const cell = table.getElementsByTagName('td')[random];
        if (cell.textContent === '') {
            cell.classList.add('mine');
            cell.innerHTML = '*';
            mines--;
        }
    }
}

const setNumbers = (table) => {
    const tdList = table.getElementsByTagName('td');
    const rowNum = table.childElementCount;
    const colNum = table.firstElementChild.childElementCount;

    for (let i = 0; i < tdList.length; i++) {
        const cell = tdList[i];
        if (!cell.textContent) {
            let counter = 0;
            const cellNumber = parseInt(cell.className);
            const parentNumber = parseInt(cell.parentElement.className);
            for (let j = parentNumber - 1; j <= parentNumber + 1; j++) {
                for (let k = cellNumber - 1; k <= cellNumber + 1; k++) {
                    if (j > 0 && k > 0 && j <= rowNum && k <= colNum && !(j === parentNumber && k === cellNumber) && document.getElementsByClassName('game-field')[0].getElementsByClassName(String(j + 'row'))[0].getElementsByClassName(String(k + 'col'))[0].textContent === '*') {
                        counter++;
                    }

                }
            }
            if (counter > 0) {
                cell.classList.add('number');
                cell.textContent = counter;
            }
        }
    }

}

const addCover = (table) => {
    const tdList = table.getElementsByTagName('td');
    for (let i = 0; i < tdList.length; i++) {
        const cover = document.createElement('div');
        cover.classList.add('cover');
        tdList[i].append(cover);
    }
}

const checkRightFlag = (mine) => {
    if (mine.getElementsByClassName('flag').length !== 0) {
        mine.classList.add('right');
    }
}

const openMines = (table) => {
    const mines = table.getElementsByClassName('mine');
    for (let i = 0; i < mines.length; i++) {
        if (mines[i].getElementsByClassName('cover').length !== 0) {
            checkRightFlag(mines[i]);
            mines[i].getElementsByClassName('cover')[0].className = 'hidden';
        }
    }
}

const removeEvents = (element) => {
    element.removeEventListener('click', showCellEvent);
    element.removeEventListener('contextmenu', setFlag);
}

const openCell = (cell) => {
    const parent = cell.parentElement;
    const isCovered = cell.getElementsByClassName('cover').length !== 0;
    const cellNumber = parseInt(cell.className);
    const parentNumber = parseInt(parent.className);

    const rowNum = document.getElementsByClassName('game-field')[0].childElementCount;
    const colNum = parent.childElementCount;

    if (isCovered) {
        if (!cell.textContent.includes('#')) {
            cell.getElementsByClassName('cover')[0].className = 'hidden';
            if (cell.textContent === '') {
                for (let i = parentNumber - 1; i <= parentNumber + 1; i++) {
                    for (let j = cellNumber - 1; j <= cellNumber + 1; j++) {
                        if (i > 0 && j > 0 && i <= rowNum && j <= colNum) {
                            openCell(document.getElementsByClassName('game-field')[0].getElementsByClassName(String(i + 'row'))[0].getElementsByClassName(String(j + 'col'))[0]);

                        }
                    }
                }
            }
            if (cell.textContent.includes('*')) {
                cell.classList.add('bang')
                openMines(document.getElementsByClassName('game-field')[0]);

                const lose = document.createElement('p');
                lose.textContent = "You lose";
                lose.classList.add('lose');

                document.getElementsByClassName('game-field')[0].after(lose);

                removeEvents(document.getElementsByClassName('game-field')[0]);
            }
        }
    }
    return 0;
}

const showCellEvent = (event) => {
    const cover = event.target
    const td = cover.parentElement;
    if (cover.tagName === 'DIV' && cover.classList.contains('cover')) {
        openCell(td);
    }
    event.stopPropagation();
}

const showCellElem = (elem) => {
    const td = elem.parentElement;
    if (elem.tagName === 'DIV' && elem.classList.contains('cover')) {
        openCell(td);
    }
}

const checkWin = (table) => {
    const mines = table.getElementsByClassName('mine');
    let counter = 0
    for (let i = 0; i < mines.length; i++) {
        if (mines[i].getElementsByClassName('cover')[0].textContent === "#") {
            counter++;
        }
    }
    if (counter === mines.length) {
        const win = document.createElement('p');
        win.textContent = "You win";
        win.classList.add('win');
        document.getElementsByClassName('container')[0].append(win);
        removeEvents(table)
    }
}

const setFlag = (event) => {
    const cover = event.target
    const flagCounter = document.getElementsByClassName('flag-counter')[0];

    if (cover.tagName === 'DIV' && cover.classList.contains('cover')) {
        if (cover.textContent === '') {
            cover.textContent = '#';
            cover.classList.add('flag');
            flagCounter.textContent = String(parseInt(flagCounter.textContent) + 1);
            checkWin(document.getElementsByClassName('game-field')[0]);
        } else {
            cover.textContent = '';
            cover.classList.remove('flag');
            flagCounter.textContent = String(parseInt(flagCounter.textContent) - 1);

        }
    }
    event.preventDefault();
}

const checkFlag = (event) => {
    if (event.target.className.includes('number')) {
        const cellNumber = parseInt(event.target.className);
        const parentNumber = parseInt(event.target.parentElement.className);
        const rowNum = document.getElementsByClassName('game-field')[0].childElementCount;
        const colNum = document.getElementsByClassName('game-field')[0].firstElementChild.childElementCount;

        let counter = 0;

        for (let i = parentNumber - 1; i <= parentNumber + 1; i++) {
            for (let j = cellNumber - 1; j <= cellNumber + 1; j++) {
                if (i > 0 && j > 0 && i <= rowNum && j <= colNum && !(i === parentNumber && j === cellNumber)) {
                    const cover = document.getElementsByClassName('game-field')[0].getElementsByClassName(String(i + 'row'))[0].getElementsByClassName(String(j + 'col'))[0];
                    if (cover !== undefined && cover.textContent.includes('#')) {
                        counter++;
                    }
                }
            }
        }
        if (counter === parseInt(event.target.textContent)) {
            openAround(event, parentNumber, cellNumber, rowNum, colNum);
        }
    }
}


const openAround = (elem, x, y, max1, max2) => {
    for (let i = x - 1; i <= x + 1; i++) {
        for (let j = y - 1; j <= y + 1; j++) {
            if (i > 0 && j > 0 && i <= max1 && j <= max2) {
                const cover = document.getElementsByClassName('game-field')[0].getElementsByClassName(String(i + 'row'))[0].getElementsByClassName(String(j + 'col'))[0].getElementsByClassName('cover')[0];
                if (cover !== undefined && !cover.textContent.includes('#')) {
                    showCellElem(cover);
                }
            }
        }
    }
}

const startGame = () => {
    if (document.getElementsByClassName('container').length > 0) {
        document.getElementsByClassName('container')[0].remove();
    }

    const container = document.createElement('div');
    container.classList.add('container');
    document.body.prepend(container);

    const row = (document.getElementsByClassName('input-row')[0].value !== '') ? document.getElementsByClassName('input-row')[0].value : 8;
    const col = (document.getElementsByClassName('input-col')[0].value !== '') ? document.getElementsByClassName('input-col')[0].value : 8;
    container.append(drowTable(row, col));

    setMines(document.getElementsByClassName('game-field')[0]);

    setNumbers(document.getElementsByClassName('game-field')[0]);

    const flagCounterLabel = document.createElement('p');
    flagCounterLabel.classList.add('text')
    flagCounterLabel.textContent = 'Flags: '
    container.prepend(flagCounterLabel)

    const flagCounter = document.createElement('span');
    flagCounter.classList.add('flag-counter');
    flagCounter.textContent = '0';

    flagCounterLabel.append(flagCounter);

    const flagCounterLabelEnd = document.createElement('span');
    flagCounterLabelEnd.classList.add('text')
    flagCounterLabelEnd.textContent = ' / ' + (document.getElementsByClassName('mine').length);
    flagCounter.after(flagCounterLabelEnd);

    addCover(document.getElementsByClassName('game-field')[0])
    document.body.getElementsByClassName('game-field')[0].addEventListener('click', showCellEvent);
    document.body.getElementsByClassName('game-field')[0].addEventListener('contextmenu', setFlag);

    document.body.getElementsByClassName('game-field')[0].addEventListener('dblclick', checkFlag);

    document.getElementsByClassName('btn')[0].textContent = 'Restart';
}

document.getElementsByClassName('btn')[0].addEventListener('click', startGame);