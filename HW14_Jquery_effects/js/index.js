'use strict'

$('a[href^="#"').click(function() {
    const href = $(this).attr('href');

    $('html, body').animate({scrollTop : $(href).offset().top}, 500);
    return false;
});

$(window).scroll( () => {
    if($('html, body').scrollTop() > window.innerHeight){
        $('.toTop').removeClass('hidden');
    } else {
        $('.toTop').addClass('hidden');
    }
})

$('.hide-section').click(()=>{
    $('#hot-news').slideToggle();
})
