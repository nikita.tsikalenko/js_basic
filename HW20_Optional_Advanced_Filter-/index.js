'use strict'
const filterCollection = (arr, filter, bool, ...params) => {
    const fArr = filter.split(' ');
    const newArr = [];
    outer: for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < fArr.length; j++) {
            if (!findInParam(fArr[j], arr[i], params)) {
                continue outer;
            }
            if (!bool) {
                break;
            }
        }
        newArr.push(arr[i]);
    }
    return newArr;
}

const findInParam = (filter, obj, params) => {
    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            findInParam(filter, obj[i], params);
        }
    } else if (typeof obj === 'object') {
        const objParams = Object.keys(obj);
        for (let i = 0; i < objParams.length; i++) {
            for (let j = 0; j < params.length; j++) {
                if (objParams[i] === params[j]) {
                    if (Array.isArray(obj[objParams[i]])) {
                        return findInParam(filter, obj[objParams[i]], params);
                    }
                    if (obj[objParams[i]] === filter) {
                        return true;
                    }
                }
            }

        }
    } else if (obj === filter) {
        return true;
    }
    return false;
}

const arr = [{
    name: 'name',
    age: '45',
    sex: 'male',
    inheritor: [{name: 'Name2', sex: 'male', age: 19,}, {name: 'Name3', sex: 'female', age: 15,}]
}, {
    name: 'name4',
    age: '40',
    sex: 'female',
    inheritor: [{name: 'Name5', sex: 'female', age: 19,}, {name: 'Name6', sex: 'male', age: 15,}]
}, {
    name: 'name6',
    age: '45',
    sex: 'female',
    inheritor: {name: 'Name2', sex: 'female', age: 19,},
}, {
    name: 'name6',
    age: '40',
    sex: 'male',
    inheritor: {name: 'Name7', sex: 'male', age: 15,},
},]
console.log(filterCollection(arr, 'name6', true, 'name'));
