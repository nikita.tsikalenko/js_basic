'use strict'
const addEvent = (arr, event, func) => {
    for (let i = 0; i < arr.length; i++) {
        arr[i].addEventListener(event, func);
    }
}

const showPassword = (event) => {
    event.target.classList.add('hidden')
    event.target.nextElementSibling.classList.remove('hidden');
    event.target.parentElement.querySelector('input').type = 'password';
}

const hidePassword = (event) => {
    event.target.classList.add('hidden')
    event.target.previousElementSibling.classList.remove('hidden');
    event.target.parentElement.querySelector('input').type = 'text';
}

document.body.getElementsByClassName('btn')[0].addEventListener('click', () => {
    const inputs = document.getElementsByClassName('password');
    const errors = document.body.getElementsByClassName('error');
    if (inputs[0].value === inputs[1].value) {
        if (errors.length > 0){
            errors[0].remove();
        }
        alert('You are welcome');
    } else {
        if (errors.length === 0) {
            const err = document.createElement('p');
            err.innerText = 'Input the same values';
            err.style.color = 'red';
            err.className = 'error';
            console.log(inputs[1])
            inputs[1].after(err);
        }
    }

})

addEvent(document.getElementsByClassName('show-password'), 'click', showPassword);
addEvent(document.getElementsByClassName('hidden-password'), 'click', hidePassword);