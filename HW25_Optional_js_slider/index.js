'use strict'

const findActive = (gallery) => {
    for (let i = 0; i < gallery.length; i++) {
        if (gallery[i].classList.contains('show')) {
            return i;
        }
    }
}

const next = () => {
    const gallery = document.getElementsByClassName('image-to-show');
    let active = findActive(gallery);

    gallery[active].classList.remove('show');
    active++;
    if (active === gallery.length) {
        active = 0;
    }
    gallery[active].classList.add('show');
}

const previous = () => {
    const gallery = document.getElementsByClassName('image-to-show');
    let active = findActive(gallery);

    gallery[active].classList.remove('show');
    active--;
    if (active < 0) {
        active = gallery.length - 1;
    }
    gallery[active].classList.add('show');
}

document.getElementsByClassName('next')[0].addEventListener('click', next);
document.getElementsByClassName('previous')[0].addEventListener('click', previous);