'use strict'

const filterBy = (arr, type) => {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
        if(typeof(arr[i]) !== type){
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

console.log(filterBy([5, 1, 'string' , true, {}], 'string'));