'use strict'

const drawCircle = () => {
    const diameter = prompt('Input diameter');
    const radius = +diameter / 2;
    for (let i = 0; i < 100; i++) {
        const circle = document.createElement('div');
        circle.style.display = 'inline-block';
        circle.style.height = circle.style.width = diameter + 'px';
        circle.style.borderRadius = radius.toString() + 'px';
        circle.style.backgroundColor = 'rgb(' + (Math.random() * 255) + ',' + (Math.random() * 255) + ',' + (Math.random() * 255) + ')';
        circle.className = 'circle';
        document.body.append(circle);
    }
}

document.body.addEventListener("click", (event) => {
        if (event.target.className === 'circle') {
            event.target.remove();
        }
    },
    true
)

document.querySelector('button').addEventListener('click', drawCircle);