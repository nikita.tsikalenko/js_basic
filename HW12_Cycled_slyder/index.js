'use strict'

let scroll;
const timerOut = document.createElement('p');

const findActive = (gallery) => {
    for (let i = 0; i < gallery.length; i++) {
        if (gallery[i].classList.contains('show')) {
            return i;
        }
    }
}

const start = () => {
    const gallery = document.getElementsByClassName('image-to-show');
    let counter = findActive(gallery);
    document.body.prepend(timerOut);

    let timer = 3000;

    scroll = setInterval(() => {
        let temp = parseInt(timer / 1000) + ':' + timer % 1000;

        timerOut.textContent = temp;
        timer -= 4;
        if (timer === 500) {
            gallery[counter].classList.add('fade')
            let temp = counter + 1;
            if (temp > gallery.length - 1) {
                temp = 0;
            }
            gallery[temp].classList.add('show');
        }
        if (timer === 0) {
            gallery[counter].classList.remove('fade');
            gallery[counter].classList.remove('show');
            counter++;
            if (counter > gallery.length - 1) {
                counter = 0;
            }
            timer = 3000;
        }
    }, 4);
}

document.getElementsByClassName('start')[0].addEventListener('click', start);

document.getElementsByClassName('stop')[0].addEventListener('click', () => {
    clearInterval(scroll);
    timerOut.remove();
});



