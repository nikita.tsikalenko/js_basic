const checkRightInputNumber = (message) => {
    while (true) {
        const num = prompt(message);
        if (!num || Number.isNaN(num)) {
            alert('Wrong input number');
            continue
        }
        return +num;
    }
}

const checkRightInputChar = (message) => {
    for (let i = 0; true; i++) {
        const sign = prompt(message);
        if (sign === '+' || sign === '-' || sign === '*' || sign === '/') {
            return sign;
        }
        alert('Wrong input sign');
    }
}
const makeOperation = (fNum1, fNum2, fChar) => {
    switch (fChar) {
        case '+': {
            return fNum1 + fNum2;
        }
        case '-': {
            return fNum1 - fNum2;
        }
        case '*': {
            return fNum1 * fNum2;
        }
        case '/': {
            return fNum1 / fNum2;
        }
        default: {
            console.log('Unknown operation');
            break;
        }
    }
}

console.log(makeOperation(checkRightInputNumber('Input first number'),
    checkRightInputNumber('Input second number'),
    checkRightInputChar('Input sign of math operation(+, -, *, /)')));


