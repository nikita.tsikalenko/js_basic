'use strict'

const tabs = document.getElementsByClassName('tabs-title');
const texts = document.getElementsByClassName('text');

for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', () => {
        for (let j = 0; j < tabs.length; j++) {
            tabs[j].className = 'tabs-title';
            texts[j].className = 'text hidden'
        }
        tabs[i].className = 'tabs-title active';
        texts[i].className = 'text show'
    })
}