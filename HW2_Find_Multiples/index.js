//Main task
while (true) {
    let num = prompt('Input integer');
    if (!!num && Number.isInteger(+num)) {
        if (num >= 5) {
            num = num - (num % 5);
            while (num > 0) {
                console.log(num);
                num -= 5;
            }
            break;
        }
        console.log('Sorry no numbers');
        break;
    }
}

//Optional task
let m;
let n;

while (!m || !n || Number.isNaN(m) || Number.isNaN(n) || m > n) {
    m = prompt('Input less number');
    n = prompt('Input larger number');
}
m = +m;
n = +n;
outer: for (; m <= n; m++) {
    for (let i = 2; i < m; i++) {
        if (m % i === 0) {
            continue outer;
        }
    }
    console.log(m);
}