'use strict'
const factorial = (n) => {
    if (n > 1) {
        n = n * factorial(n - 1);
    }
    return n;
}

const checkRightInputNumber = (message) => {
    let num = prompt(message);
    while (true) {
        if (!!num && Number.isInteger(+num)) {
            return num;
        }
        alert('Wrong input number');
        num = prompt(message, String(num));
    }
}

console.log(factorial(checkRightInputNumber('Input integer number')));